const fs = require('fs');
const path = require('path');

function createEachFile(filePath, fileData, callback) {
    fs.writeFile(filePath, fileData, (err) => {
        if (err) {
            console.error(`Error creating file ${filePath}:`, err);
        } else {
            console.log(`Created file: ${filePath}`);
            callback();
        };
    });
};

function deleteFiles(allPaths, callback) {
    let filesDeleted = 0;

    allPaths.forEach((path) => {
        fs.unlink(path, (err) => {
            if (err) {
                console.error(`Error deleting file ${path}:`, err);
            } else {
                filesDeleted += 1;
                if (filesDeleted === allPaths.length) {
                    callback();
                };
            };
        });
    });
};

function fsProblem1(pathOfDirectory, numberOfFiles) {
    const allPaths = [];

    fs.mkdir(pathOfDirectory, (err) => {
        if (err) {
            console.error(`Error creating directory ${pathOfDirectory}:`, err);
        } else {
            let filesCreated = 0;

            for (let index = 0; index < numberOfFiles; index++) {
                const fileName = `file_${index + 1}.json`;
                const filePath = path.join(pathOfDirectory, fileName);
                allPaths.push(filePath);
                const fileData = `Data ${index + 1}`;

                createEachFile(filePath, fileData, () => {
                    filesCreated += 1;
                    if (filesCreated === numberOfFiles) {
                        deleteFiles(allPaths, () => {
                            console.log(`Deleted all files in ${pathOfDirectory}`);
                        });
                    };
                });
            };
        };
    });
};

module.exports = fsProblem1;








