const fs = require('fs');

function fsProblem2() {
    const inputFile = 'lipsum.txt';

    fs.readFile(inputFile, 'utf8', (err,data) => {
        if (err) {
            console.error('Error while reading input file:', err);
        } else {

            const toUpperCase = (data) => {
                data = data.toUpperCase();
                return data;
            };
            const upperCaseContent = toUpperCase(data);
            fs.writeFile('upperCase.txt', upperCaseContent, (err) => {
                if (err) {
                    console.error('Error while writing the data:', err);
                } else {
                    console.log('Written to upperCase.txt');
                    fs.appendFile('filenames.txt', 'upperCase.txt\n', (err) => {
                        if (err) {
                            console.error('Error while writing the file:', err);
                        } else {
                            console.log('Filename written to filenames.txt');

                            const toLowerCaseAndSplit = (data) => {
                                data = data.toLowerCase().split('. ');
                                return data;
                            };
                            const lowerCaseAndSplitContent = toLowerCaseAndSplit(data);
                            fs.writeFile('lowerCaseAndSplit.txt', lowerCaseAndSplitContent.join('/n'), (err) => {
                                if (err) {
                                    console.error('Error while writing the data:', err);
                                } else {
                                    console.log('Written to lowerCaseAndSplit.txt');
                                    fs.appendFile('filenames.txt', 'lowerCaseAndSplit.txt\n', (err) => {
                                        if (err) {
                                            console.error('Error while writing the file:', err);
                                        } else {
                                            console.log('Filename written to filenames.txt');

                                            const filesToSort = ['upperCase.txt', 'lowerCaseAndSplit.txt'];
                                            const sortFiles = (files) => {
                                                const filesContent = files.map((file) => {
                                                    return fs.readFileSync(file, 'utf8')
                                                });
                                                const sortContent = filesContent.sort();
                                                const joinContent = sortContent.join('\n');
                                                fs.writeFile('sorted.txt', joinContent, (err) => {
                                                    if (err) {
                                                        console.error('Error while writing the data:', err);
                                                    } else {
                                                        console.log('Written to sorted.txt');
                                                        fs.appendFile('filenames.txt', 'sorted.txt\n', (err) => {
                                                            if (err) {
                                                                console.error('Error while writing the file:', err);
                                                            } else {
                                                                console.log('Filename written to filenames.txt');

                                                                fs.readFile('filenames.txt', 'utf8', (err,data) => {
                                                                    if (err) {
                                                                        console.error('Error while reading the file:', err);
                                                                    } else {
                                                                        const filesToDelete = data.split('\n').filter(file => {
                                                                            return file.trim() !== '';
                                                                        });
                                                                        filesToDelete.forEach((file) => {
                                                                            const trimmedFileNames = file.trim();
                                                                            fs.unlink(trimmedFileNames, (err) => {
                                                                                if (err) {
                                                                                    console.error('Error while deleting file:', err);
                                                                                } else {
                                                                                    console.log('File deleted: ', file)
                                                                                };
                                                                            });
                                                                        });
                                                                    };
                                                                });
                                                            };
                                                        });
                                                    };
                                                });
                                            };

                                            sortFiles(filesToSort);
                                        };
                                    });
                                };
                            });
                        };
                    });
                };
            });
        };
    });
};

module.exports = fsProblem2;